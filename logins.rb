def rand_time
  Time.at(rand * Time.now.to_i)
end

def year_occurences_by_name(logins, persons, new_logins = {})
  logins.map { |key, value| new_logins[persons.to_h[key]] = value.each_with_object(Hash.new(0)) { |name, hash| hash[name.year] += 1} }
  new_logins
end


persons = [[2, 'matayo'], [1, 'nico'], [0, 'angelo'], [3, 'luca']]

logins = { # ugly on purpose
  0 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
  1 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
  2 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
  3 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
}

# Write your solution below. Keep above code intact.
new_logins = year_occurences_by_name(logins, persons)


puts new_logins
