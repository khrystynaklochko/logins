require_relative '../logins'

RSpec.describe "creates hash with proper structure" do

  let(:persons) { [[2, 'matayo'], [1, 'nico'], [0, 'angelo'], [3, 'luca']] }
  let(:logins) { { 0 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
                   1 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
                   2 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
                   3 => [rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time, rand_time],
               } }

  it "creates hash with new keys" do
    expect(year_occurences_by_name(logins, persons)).to include('angelo')
    expect(year_occurences_by_name(logins, persons).keys.count).to eq(4)
  end

  it "count occurences by year" do
    expect(year_occurences_by_name(logins, persons)['luca'].keys.first.class).to be(Fixnum)
    expect(year_occurences_by_name(logins, persons)['nico'].values.first.class).to be(Fixnum)
  end
end
